function AntiLRM(mu,sigma,Phi,Nsim2)
	#Algoritmo per la simulazione del sottostante tramite modello di KOU
	#(double Exp Jumps)
	#Anche in questo caso simulo le traiettorie intere poi prendo la scadenza.
	## Initialize
	#srand(1);
	N1=randn(Int(Nsim2/2.0),1);
	N1=[N1;-N1]
	FF(x,mu,sigma)=log.(exp.(-0.5*((x-mu).^2)./(sigma.*sigma))./sqrt(2*pi*sigma*sigma));
	OutPut=mean(@. Phi(N1,DualNumbers.value(mu),DualNumbers.value(sigma))*((FF(DualNumbers.value(mu)+DualNumbers.value(sigma)*N1,mu,sigma))));

return OutPut;
end