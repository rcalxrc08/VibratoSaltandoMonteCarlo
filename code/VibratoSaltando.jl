using DualNumbers;
include("AssetKou.jl");
include("AntiLRM.jl");
function VibratoSaltando(spot=100.0,K=80.0,r=0.01,T=2.0,sigma=0.2,Nsteps=30,NSim=10000,Nsim2=1000)
	#Algoritmo per la simulazione del sottostante tramite modello di KOU
	#(double Exp Jumps)
	#Anche in questo caso simulo le traiettorie intere poi prendo la scadenza.
	## Initialize
	#srand(1234);
	Lam=2.0;
	P=0.2;
	LamP=3.0;
	LamM=4.0;
	LRMOut=Dual.(0.0,0.0);
	Payoff=Dual.(0.0,0.0);
	dt=T/Nsteps;
	CoeffRand=rand(1:10000);
	#CoeffRand=13;
	(S1,Tj1,Sj1)=AssetKOU(spot,T,r,sigma,Lam,P,LamP,LamM,Nsteps,NSim);
	drift_RN=-sigma^2/2-Lam*(P/(LamP-1)-(1-P)/(LamM+1));
	mu1=@. log(Sj1)+(r+drift_RN)*(T-Tj1);
	Sigma1=@. sigma*sqrt(T-Tj1);
	#max.(ST-K,0)
	Phi(N,Mu,Sigma)=@. max(exp(Mu+Sigma*N)-K,0);
	for i=1:NSim
		LRMOut+=(AntiLRM(mu1[i],Sigma1[i],Phi,Nsim2));
		N12=randn(Nsim2,1);
		Payoff+=sum(Phi(N12,mu1[i],Sigma1[i]))/Nsim2;
	end
	Price=exp(-r*T).*(Payoff)/NSim;
	println("Price =  $(Price.value)		Vega =  $(Price.epsilon)")
	PP1=exp(-r*T).*(LRMOut)/NSim;
	println("Vega Vibrato = $(PP1.epsilon)")
	return PP1.epsilon;
end
#plot(t,S);
