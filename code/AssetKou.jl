using Distributions,DualNumbers;
function AssetKOU(spot=100.0,T=2.0,r=0.01,sigma=0.2,lambda=2.0,p=0.2,lambdap=3.0,lambdam=4.0,Nsteps=30,NSim=10000)
	#Algoritmo per la simulazione del sottostante tramite modello di KOU
	#(double Exp Jumps)
	#Anche in questo caso simulo le traiettorie intere poi prendo la scadenza.
	## Initialize
	#srand(1234);
	STimes=zeros(NSim,1);


	zero__=r*sigma*lambda*lambdap*lambdam*0
	X=zeros(typeof(zero__),NSim,Nsteps+1);
	dt=T/Nsteps; 
	t=collect(0.0:dt:T);
	STimes=Dual.(STimes,0);
	## Simulate
	# -psi(-i)
	drift_RN=-sigma*sigma/2.0-lambda*(p/(lambdap-1)-(1-p)/(lambdam+1)); #calcolo drift in RN, cambia a seconda dell' AF che scelgo
	D1=Poisson(lambda*T);
	M1=Exponential(1/lambdap);
	M2=Exponential(1/lambdam);
	vNT=Int.(quantile(D1,rand(NSim,1))); #invariante
	Z=randn(NSim,Nsteps);
	JumpTimes=zeros(NSim);
	for ii=1:NSim
		NT=vNT[ii];
		# Simulate the number of jumps (conditional simulation)
		tjumps=sort(rand(NT)*T);
		NJump=0;
		#if length(tjumps)>0
		#	JumpTimes[ii]=tjumps[NT];
		#end
		
		for i=1:Nsteps
			# Simulate the Continuous Part
			X[ii,i+1]=X[ii,i]+(r+drift_RN)*dt+sigma*sqrt(dt)*Z[ii,i]; # Metodo Per SDE.
			# Add the jump size
			for j=1:NT
			   if tjumps[j]>t[i] && tjumps[j]<=t[i+1] #cerco istante di salto nella griglia dei tempi.
				  NJump+=1;
				  u=rand(1)[1]; #simulo Uniforme([0,1])
				  if (u<p) # positive 
					  jump_size=quantile(M1,rand(1)[1]);
				  else # negative jump
					  jump_size=-1.0*quantile(M2,rand(1)[1]);#ricavo intensità salto,cambia a seconda dell' AF che scelgo
				  end
				  X[ii,i+1]=X[ii,i+1]+jump_size; #aggiungo componente di salto.
				  if ((NJump==NT)*(NT>0))
					JumpTimes[ii]=t[i];
					STimes[ii]=spot.*exp.(X[ii,i+1]);
				  end
			   end
			end
		end 
		if (NJump==0)
			STimes[ii]=spot;
			JumpTimes[ii]=0;
		end
	end
	## Conclude
	#JumpTimes=T.-JumpTimes;
	S=@. spot*exp(X);
	return (S,JumpTimes,STimes);
end
#plot(t,S);